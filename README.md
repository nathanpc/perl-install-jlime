# perl-install-jlime

Perl installation script for [Jlime](http://jlime.com/) devices. This script is supposed to install all the needed modules/packages, like [CPAN](http://cpan.org/), to have a usable Perl environment on your Jlime.


## Size

Many will think that it's a lot of packages to get installed. Yes, it's **381 packages**, but if you look at the [Jlime repos](http://www.jlime.com/downloads/repository/mongo/feed/base/) the majority of them are **~5kb** and some **~18-10kb**. At the end you'll get a perfect Perl environment (with CPAN and all the other required modules for almost every useful Perl script) for less than **4MB**.