#!/bin/usr/perl
# install.pl
# Install all the needed Perl modules.

# Get the packages list.
@packages = `ipkg list | grep perl-module`;

sub clean_raw_list {
	# Extract only the package name from the list.
	foreach (@packages) {
		$_ = substr($_, 0, index($_, " "));
	}
}

clean_raw_list();
system("ipkg", "install", @packages);